package com.spring.springbatch.batch;

import com.spring.springbatch.model.UserDto;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserDtoFieldSetMapper implements FieldSetMapper {
    @Override
    public UserDto mapFieldSet(FieldSet fieldSet) throws BindException {
        List<String> defaultColumns = Arrays.asList("name", "salary", "department");
        UserDto userDto = new UserDto();
        userDto.setName(fieldSet.readString("name"));
        userDto.setSalary(fieldSet.readInt("salary"));
        Map<String, String> collect1 = fieldSet.getProperties()
                .entrySet()
                .stream()
                .filter(e -> !defaultColumns.contains(e.getKey()))
                .collect(Collectors.toMap(x->x.getKey().toString(), x ->x.getValue().toString()));
        userDto.setMap(collect1);
        return userDto;
    }

}