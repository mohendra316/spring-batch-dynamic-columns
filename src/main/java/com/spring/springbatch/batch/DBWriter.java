package com.spring.springbatch.batch;

import com.spring.springbatch.model.Department;
import com.spring.springbatch.model.User;
import com.spring.springbatch.model.UserDto;
import com.spring.springbatch.repository.DepartmentRepository;
import com.spring.springbatch.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class DBWriter implements ItemWriter<UserDto> {
    private final UserRepository userRepository;
    private final DepartmentRepository departmentRepository;
    private static boolean isSetDepartment;
    private JobExecution jobExecution;

    @Autowired
    public DBWriter(UserRepository userRepository, DepartmentRepository departmentRepository) {
        this.userRepository = userRepository;
        this.departmentRepository = departmentRepository;
    }

    @Override
    public void write(List<? extends UserDto> users) {
        JobParameters parameters = jobExecution.getJobParameters();
        String departmentName =  parameters.getString("department");
        log.info("Inserting value of user with data size: {}", users.size());
        userRepository.saveAll(users
                .stream()
                .map(u -> {
                    User user1 = new User();
                    user1.setName(((UserDto) u).getName());
                    user1.setSalary(((UserDto) u).getSalary());
                    user1.setProperties(((UserDto) u).getMap());
                    return user1;
                }).collect(Collectors.toList()));
    }

    @BeforeStep
    public void beforeStep(StepExecution stepExecution) {
        jobExecution = stepExecution.getJobExecution();
    }
}




