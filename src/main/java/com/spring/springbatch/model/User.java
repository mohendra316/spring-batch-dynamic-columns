package com.spring.springbatch.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Map;

@Entity
@Getter
@Setter
@ToString
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private Integer salary;

    @ManyToOne
    private Department department;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name = "propKey")
    @Column(name = "propValue")
    private Map<String, String> properties;
}
