package com.spring.springbatch.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.ManyToOne;
import java.util.Map;

@Getter
@Setter
public class UserDto {

    private String name;
    private Integer salary;
    private Department department;
    private Map<String,String> map;
}
